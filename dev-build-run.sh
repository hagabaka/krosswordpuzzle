#!/bin/sh

mkdir -p build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr ..
cmake --build .
rm -rf pkg
make DESTDIR=pkg install

XDG_DATA_DIRS="$PWD/pkg/usr/share:$XDG_DATA_DIRS" pkg/usr/bin/krosswordpuzzle